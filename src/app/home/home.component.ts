import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { DialogDataComponent } from '../dialog-data/dialog-data.component';
import { BackendApiService } from '../services/backend-api.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ BackendApiService ]
})

export class HomeComponent implements OnInit {
  items: Array<Object>;
  constructor(
    private _router: Router,
    private dialog: MatDialog,
    private api: BackendApiService
  ) {}

  ngOnInit() {
    this.api.getList().subscribe( (res: Array<Object>) => {
      this.items = res;
    });
  }

  getItems() {
    const data = [ ];
    for (let i = 1; i <= 12; i++ ) {
      data.push({
        id: i,
        lableTitle: `Title ${ i }`,
        detailAbout: `Detail About-${ i }`,
        fab: ( i % 3 && i % 5 ) ? false : true,
      });
    }
    return data;
  }

  navigateDetails(details) {
    this._router.navigate( ['/about-us'], { queryParams: details} );
  }

  toggleFab(item: Object) {
    item['fab'] = !item['fab'];
  }

  openDialog(item) {
    const dialogRef = this.dialog.open(DialogDataComponent, {
      data: item,
      width: '40rem',
      panelClass: 'info-dialog',
      disableClose: true
    });

    dialogRef.afterClosed().subscribe(result => {});
  }

}
