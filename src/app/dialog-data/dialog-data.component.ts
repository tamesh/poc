import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';

export interface DialogData {
  details: {
    id: Number,
    lableTitle: String,
    detailAbout: String,
    fab: Boolean
  };
}

@Component({
  selector: 'app-dialog-data',
  templateUrl: './dialog-data.component.html',
  styleUrls: ['./dialog-data.component.css']
})

export class DialogDataComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DialogDataComponent>,
    @Inject(MAT_DIALOG_DATA) public details: DialogData
  ) {}

  ngOnInit() {

  }

  closeDialog(): void {
    this.dialogRef.close();
  }
}


