import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './app.material.module';

import { AppComponent } from './app.component';
import { AppRoutingModule, routingComponents } from './app.routing.module';
import { DialogDataComponent } from './dialog-data/dialog-data.component';


@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    DialogDataComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
  ],
  providers: [],
  entryComponents: [ DialogDataComponent ],
  bootstrap: [AppComponent]
})
export class AppModule { }
