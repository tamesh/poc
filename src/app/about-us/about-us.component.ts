import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit {
  detailsList: Object;
  constructor(
    private _router: Router,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    // atob
    // btoa
    this.detailsList = this._route.snapshot.queryParams;
  }

  backToTiles() {
    this._router.navigate(['/home']);
  }

}
